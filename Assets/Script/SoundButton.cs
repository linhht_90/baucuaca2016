﻿using UnityEngine;
using System.Collections;

public class SoundButton : MonoBehaviour {
	public UISprite sprite;
	int t=0;
	// Use this for initialization
	void Start () {
		t=PlayerPrefs.GetInt(Key.KEY_SOUND_ON_OFF);
		if(t==0)
		{
			SoundManager.mutedMusic=false;
		}
		else
		{
			SoundManager.mutedMusic=true;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(t==0)
		{
			sprite.spriteName="sound_on";
		}else
		{
			sprite.spriteName="sound_off";
		}
	}
	public void OnclickButton()
	{
		if(t==0)
		{
			SoundManager.mutedMusic=true;
			t=1;
		}
		else
		{
			SoundManager.mutedMusic=false;
			t=0;
		}
		PlayerPrefs.SetInt(Key.KEY_SOUND_ON_OFF,t);
	}
}
