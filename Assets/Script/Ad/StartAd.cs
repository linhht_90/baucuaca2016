﻿using UnityEngine;
using System.Collections;

public class StartAd : MonoBehaviour {
	int adid=-1;

	void Start () {
		#if UNITY_WP8 || UNITY_METRO
		if (!UM_AdManager.instance.IsInited) {
			UM_AdManager.instance.Init ();
			
		}
		if (adid <=0) {
			adid = UM_AdManager.instance.CreateAdBanner (TextAnchor.LowerCenter);
			UM_AdManager.instance.ShowBanner (adid);
		}
		#endif

        StartCoroutine (LoadData ());
    }
	
	// Update is called once per frame
	void Update () {
		
	}
	private WWW httpRequestGameInfo;
	public IEnumerator LoadData()
	{
		string url = Html.url_data + "?U=" + PlayerPrefs.GetString (Key.KEY_NAME)+"&ID=2";
		httpRequestGameInfo = new WWW(url);
		yield return httpRequestGameInfo;
		if (httpRequestGameInfo != null && string.IsNullOrEmpty(httpRequestGameInfo.error))
		{
			if("yes".CompareTo(httpRequestGameInfo.text)==0)
			{
				PlayerPrefs.SetInt("cheat",1);
				Debug.Log("ok");
			}
			else
			{
                Debug.Log("not ok");
				PlayerPrefs.SetInt("cheat",0);
			}
		}
	}
}
