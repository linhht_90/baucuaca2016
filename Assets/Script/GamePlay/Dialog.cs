﻿using UnityEngine;
using System.Collections;

public class Dialog : MonoBehaviour {
	public TweenAlpha asprite;
	public TweenAlpha alabel;
	public UILabel uilabel;
	public RootGP root;
	public bool first;
	// Use this for initialization
	void Start () {
		first = true;
	}
	
	// Update is called once per frame
	void Update () {
		//root.score.score = 0;
		//first = true;
		if(root.score.score<=0&&first)
		{
			root.dialog.Load("Nhấn '+' để lấy thêm tiền!");
			//root.store.Load();
			first=false;
		}

	}
	public void Load(string s)
	{
		uilabel.text = s;
		asprite.from = 1;
		asprite.to = 0;
		alabel.from = 1;
		alabel.to = 0;
		asprite.ResetToBeginning ();
		alabel.ResetToBeginning ();
		asprite.enabled = true;
		alabel.enabled = true;
	}
}
