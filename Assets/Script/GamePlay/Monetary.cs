﻿using UnityEngine;
using System.Collections;

public class Monetary : MonoBehaviour {
	public int type;
	public RootGP root;
	public int lastChoise;
	public GameObject scroll;
	//public Bet bet;
	TweenPosition pos;
	Vector3 from,to;
	public string[] arrayItem={"bau","cua","tom","ca","ga","nai"};
	public int bau,cua,tom,ca,ga,nai;

	// Use this for initialization
	void Start () {
		pos = GetComponent<TweenPosition> ();

		from = new Vector3 (0, -250, 0);
		to = new Vector3 (0, -110, 0);
		lastChoise = 5;
		SetChangeStatusCoin (5);
		Reset ();

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void Load()
	{
    
		pos.from = from;
		pos.to = to;
		pos.ResetToBeginning ();
		pos.enabled = true;

		//root.score.score = 10;
	}
	public void UnLoad()
	{
		//root.board.RemoveAllDisable ();

		pos.from = to;
		pos.to = from;
		pos.ResetToBeginning ();
		pos.enabled = true;

	}
	public void OnClick1()
	{
		SetValue (-1,1);
		SetChangeStatusCoin (1);
	}
	public void OnClick2()
	{
		SetValue (-1,2);
		SetChangeStatusCoin (2);
	}
	public void OnClick5()
	{
		SetValue (-1,5);
		SetChangeStatusCoin (5);
	}
	public void OnClick10()
	{
		SetValue (-1,10);
		SetChangeStatusCoin (10);
	}
	public void OnClick20()
	{
		SetValue (-1,20);
		SetChangeStatusCoin (20);
	}
	public void OnClick40()
	{
		SetValue (-1,40);
		SetChangeStatusCoin (40);
	}
	void SetChangeStatusCoin(int num)
	{
		int[] uis = {1,2,5,10,20,40}; 
		foreach (int i in uis) {
			GameObject g = GameObject.FindGameObjectWithTag ("coi" + i);
			UILabel label = g.GetComponentInChildren<UILabel> ();
			label.color=Color.white;
			//ui.isEnabled=true;
		}
		GameObject g1 = GameObject.FindGameObjectWithTag ("coi" + num);
		UILabel label1 = g1.GetComponentInChildren<UILabel> ();
		label1.color = Color.green;
	}
	public GameObject SetNumBet(string name,int num)
	{
		GameObject g = GameObject.FindGameObjectWithTag ("c" + name);
		UILabel label = g.GetComponentInChildren<UILabel> ();
		UISprite sprite = g.GetComponentInChildren<UISprite> ();
		label.text = "" + num;
		if(num==0)
		{
			label.enabled=false;
			sprite.enabled=false;
		}
		else
		{
			label.enabled=true;
			sprite.enabled=true;
		}
		return g;
	}
	public void Reset()
	{
		bau = cua = tom = ca = ga = nai = 0;
		foreach(string s in arrayItem)
		{
			SetNumBet(s,0);

		}
	}
	int maxCoin=75;
	public void SetValue(int op,int num)
	{

		if(op==0&&lastChoise!=0)
		{
			num=lastChoise;
		}
		lastChoise = num;
		if (root.score.score <= 0) {
			root.dialog.Load("Không đủ tiền!!!");
			return;
		}
		else
		{
			SoundManager.playOneShot(SoundManager.Instance.coin);
		}
		if((bau+cua+tom+ca+ga+nai)>=maxCoin)
		{
			//TODO
			root.dialog.Load("Đã cược tối đa!!!");

			return;
		}
		root.score.IncreateScore (num*-1);
		switch (type) {
		case Const.BAU:
			//TODO:
			bau+=num;
			SetNumBet("bau",bau);
			break;
		case Const.CUA:
			cua+=num;
			SetNumBet("cua",cua);
			break;
		case Const.TOM:
			tom+=num;
			SetNumBet("tom",tom);
			break;
		case Const.CA:
			ca+=num;
			SetNumBet("ca",ca);
			break;
		case Const.GA:
			ga+=num;
			SetNumBet("ga",ga);
			break;
		case Const.NAI:
			nai+=num;
			SetNumBet("nai",nai);
			break;
		}

	}
	public void CheckResult()
	{
		//Cheat in here
        //if(root.score.score<=100&&root.board.select>=0)
        //{
        //    GenRandom r=new GenRandom();
        //    root.dines.SetDine(r.GetRandom(root.board.select),1);

        //}
		//end

		int scr=0;
		for(int i=0;i<6;i++)
		{
			int mux=root.dines.IsWin(i);
			switch (i) {
			case Const.BAU:
				//TODO:
				root.reward.Add("bau",bau,mux);
				scr=scr+bau*(mux+1);
				break;
			case Const.CUA:
				root.reward.Add("cua",cua,mux);
				scr=scr+cua*(mux+1);
				break;
			case Const.TOM:
				root.reward.Add("tom",tom,mux);
				scr=scr+tom*(mux+1);
				break;
			case Const.CA:
				root.reward.Add("ca",ca,mux);
				scr=scr+ca*(mux+1);
				break;
			case Const.GA:
				root.reward.Add("ga",ga,mux);
				scr=scr+ga*(mux+1);
				break;
			case Const.NAI:
				root.reward.Add("nai",nai,mux);
				scr=scr+nai*(mux+1);
				break;
			}
		}
		if(scr>=20)
		{
			SoundManager.playOneShot(SoundManager.Instance.welldone);
		}
		SoundManager.playOneShot(SoundManager.Instance.coin);
		root.board.select = -1;
		root.score.IncreateScore (scr);
	}
}
