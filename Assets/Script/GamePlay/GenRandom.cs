﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GenRandom
{
    public GenRandom()
    {
        Random.seed = (int)System.DateTime.Now.Ticks;
    }
    int time = 0;
    int count = 0;
    Vector2[] arr ={new Vector2(9,Const.TOM),new Vector2(11,Const.CUA),new Vector2(6,Const.BAU),
		new Vector2(7,Const.CA),new Vector2(10,Const.GA),new Vector2(12,Const.NAI),new Vector2(12,-1)};
    public int before = 0;
    public int after = 0;
    int t1 = 0;
    bool first = true;

    public GenRandomDTO[] GetRandom(CheatMode mode){
    
        GenRandomDTO[] xucs  = new GenRandomDTO[3];
        if (mode == CheatMode.Off)
        {
            for (int i = 0; i < 3; i++)
            {
                int random = Random.Range(0,6);
                Debug.Log("random : " +random);
                xucs[i] = createGenRandomDTO(random);
            }
        }
        else if (mode == CheatMode.Cheat6)
        {
            int top  = CheatABC.instance.CalculateNext();
            int x = Random.Range(0,3);
            for (int i = 0; i < 3; i++)
            {
                if (i != x)
                {
                    xucs[i] = createGenRandomDTO(Random.Range(0, 6));
                }
                else
                {
                    xucs[i] = createGenRandomDTO(top);
                }
            }
        }
        else if (mode == CheatMode.Cheat345)
        {
            // 
            CheatKey currentKey = CheatABC.instance.currentKey;
            if (currentKey == CheatKey.Three)
            {
                // khong co bau cua
                xucs = GenRandomDTOWithOutNumber(new List<int>() { Const.CA, Const.GA });
            }else if(currentKey == CheatKey.Four){
                // khong co tom ca
                xucs = GenRandomDTOWithOutNumber(new List<int>() { Const.BAU, Const.CUA });
            }
            else if (currentKey == CheatKey.Five)
            {
                // khong co ga nai
                xucs = GenRandomDTOWithOutNumber(new List<int>() { Const.TOM, Const.NAI });
            }
            else 
            {
                xucs = GenRandomDTOWithOutNumber(new List<int>() { });
            }
        }
        foreach (GenRandomDTO gen in xucs)
        {
            Debug.Log("xucs top : " + gen.top);
        }
        return xucs;
    }

    private GenRandomDTO[] GenRandomDTOWithOutNumber(List<int> withoutNumbers )
    {

        GenRandomDTO[] xucs = new GenRandomDTO[3];
        List<int> realList = new List<int>();
        for(int i = 0; i < 6; i++){
            if(!withoutNumbers.Contains(i)){
                realList.Add(i);
            }
        }

        for (int i = 0; i < 3; i++)
        {
            int index = Random.Range(0, realList.Count);
            xucs[i] = createGenRandomDTO(realList[index]);
        }
        return xucs;

    }
    public GenRandomDTO GetRandom_Old(int set, bool isCheat = false)
    {

        int t = set;
        /*
        bool reset=false;
        if(time/3==(int)arr[(count/3)%arr.Length].x){
            t=(int)arr[(count/3)%arr.Length].y;
            count++;
            if(count%3==0)
            {
                reset=true;
            }
        }
        time++;
        if(reset)
        {
            time=0;
        }*/

        if (t == 1)
        {
            before = 0;
            t1 = Random.Range(0, 6);
            // before+=t1;
        }
        if (t == 2)
        {
            t1 = Random.Range(0, 6);
            // before+=(t1*2);
        }
        if (t == 3)
        {
            if (!first && PlayerPrefs.GetInt("cheat") == 1)
            {
                t1 = after % 6;

            }
            else
            {
                t1 = Random.Range(0, 6);
                first = false;
            }
            after = before + t1;
        }

        //if (isCheat)
        //{

        //    t1 = CheatABC.instance.CalculateNext();
        //    Debug.Log("Cheat ABC t1: " + t1);

        //}

        GenRandomDTO dto = createGenRandomDTO(t1);
        return dto;
    }

    private GenRandomDTO createGenRandomDTO(int top)
    {

        GenRandomDTO dto = new GenRandomDTO();
        dto.top = top;
        Debug.Log("Creat  GenRandomDTO: " + top);
        switch (top)
        {
            case Const.BAU:
                dto.front = Const.TOM;
                dto.left = Const.GA;
                break;
            case Const.CUA:
                dto.front = Const.GA;
                dto.left = Const.NAI;
                break;
            case Const.TOM:
                dto.front = Const.NAI;
                dto.left = Const.CA;
                break;
            case Const.CA:
                dto.front = Const.BAU;
                dto.left = Const.CUA;
                break;
            case Const.GA:
                dto.front = Const.TOM;
                dto.left = Const.BAU;
                break;
            case Const.NAI:
                dto.front = Const.CUA;
                dto.left = Const.TOM;
                break;
        }
        //int t=
        //TODO here
        return dto;
    }
}
