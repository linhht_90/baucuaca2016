﻿using UnityEngine;
using System.Collections;

public class Dice : MonoBehaviour {

	public Up up;
	public UISprite xoc;
	public RootGP root;
	//public Monetary mone;
	public int status;
	// Use this for initialization

    private float timeForXoc = 0.6f;
    private float timeHodler = 0;
	void Start () {
		//up.Load ();
		status = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (status == 1) {
            xoc.spriteName = "mo_moi"; //Mo Image
            timeHodler += Time.deltaTime;
		} else {
            xoc.spriteName = "xoc_moi"; // Xoc Image dat nham ten ^^
		}

	}
	public void Xoc()
	{
		if (status == 0) {
            //Xoc
			up.Load ();
			status=1;
			root.mone.Reset();
			root.reward.Reset();
			SoundManager.playOneShot(SoundManager.Instance.shake);
            timeHodler = 0;
		} else {

        
            // Mo
			up.UnLoad();
			status=0;
            if (timeHodler < timeForXoc) return;
            // Thay doi: thuc hien xoc khi mo nap
            root.dines.GenNew();

			//Check result here;
			root.mone.CheckResult();		
	 
		}
	}
	public void OnFinishMove()
	{
        return;
		if (status == 1) {
            Debug.Log("OnFinishMove----------------------------------");
			
		}
	}
}
