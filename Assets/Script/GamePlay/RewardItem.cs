﻿using UnityEngine;
using System.Collections;

public class RewardItem : MonoBehaviour {

	TweenPosition pos;
	UILabel label;
	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void SetWin(string name,int num)
	{
		pos = GetComponent<TweenPosition> ();
		label = GetComponentInChildren<UILabel> ();
		label.text = "" + num;
		GameObject t = GameObject.FindGameObjectWithTag (name);
		transform.localPosition = t.transform.localPosition;
		pos.to = t.transform.localPosition*0.7f;
		pos.from = new Vector3 (0, 70, 0);
		pos.ResetToBeginning ();
		pos.enabled = true;
		 
		//TODO:Generate the coin when the player can get some rewards.
	}
	public void SetLose(string name,int num)
	{
		pos = GetComponent<TweenPosition> ();
		label = GetComponentInChildren<UILabel> ();
		label.text = "" + num;
		GameObject t = GameObject.FindGameObjectWithTag (name);
		pos.to =new Vector3 (0, 70, 0); 
		pos.from = t.transform.localPosition*0.7f;
		pos.ResetToBeginning ();
		pos.enabled = true;
		//TODO:Get the money from the player
	}
	public void Reset()
	{
		//TODO: Detroy all the coin and item in list
	}
}
