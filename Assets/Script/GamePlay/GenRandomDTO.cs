﻿using UnityEngine;
using System.Collections;

public class GenRandomDTO  {

	public int front;
	public int top;
	public int left;
	public int right;
	public int back;
	public int bottom;
}
