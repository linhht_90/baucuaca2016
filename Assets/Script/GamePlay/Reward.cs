﻿using UnityEngine;
using System.Collections;

public class Reward : MonoBehaviour {
	ArrayList list;
	public RootGP root;
	// Use this for initialization
	void Start () {
		list = new ArrayList ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void Add(string name,int num,int type)
	{
		if(num==0)
		{
			return;	
		}
		GameObject instance =(GameObject) Instantiate(Resources.Load("RewardItem", typeof(GameObject)));
		RewardItem item = instance.GetComponent<RewardItem> ();
		instance.transform.parent = transform;
		instance.transform.localScale = new Vector3 (1, 1, 1);
		if(type>0)
		{
			item.SetWin(name,num*type);
		}
		else
		{
			//t.
			root.mone.SetNumBet(name,0);
			item.SetLose(name,num);
		}
		list.Add (instance);
	}
	public void Reset()
	{
		foreach(GameObject b in list)
		{
			GameObject.DestroyImmediate(b);
		}
		list.Clear ();
	}
}
