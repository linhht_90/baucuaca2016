using UnityEngine;
using System.Collections;

public class Up : MonoBehaviour {
	public TweenPosition spring;
	public GameObject disk;
	Vector3 from1,to1;
	// Use this for initialization
	void Start () {
		from1 = new Vector3 (0, 300, 0);
		to1 = disk.transform.localPosition;
		//spring = GetComponent<TweenPosition> ();
		//Load ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void Load()
	{

		spring.from = from1;
		spring.to = to1;
		spring.ResetToBeginning ();
		spring.enabled = true;

	}
	public void UnLoad()
	{

		spring.from = to1;
		spring.to = from1;
		spring.ResetToBeginning ();
		spring.enabled = true;
		//spring.target = new Vector3 (0, 500, 0);
	}
}
