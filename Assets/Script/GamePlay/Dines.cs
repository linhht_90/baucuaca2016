﻿using UnityEngine;
using System.Collections;

public class Dines : MonoBehaviour
{

    public static int sum = -1; // -1 for first time
    GenRandom gen;
    public ArrayList result;
    // Use this for initialization
    void Start()
    {
        gen = new GenRandom();
        result = new ArrayList();
        GenNew();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void GenNew()
    {
        result.Clear();
        SetDines();
    }
    void SetDines()
    {
        GenRandomDTO[] dtos = new GenRandomDTO[3];

        dtos = gen.GetRandom(CheatABC.instance.GetCurrCheatMode());
        for (int i = 0; i < dtos.Length; i++)
        {
            SetDine(dtos[i], i+1);
        }
        int sum = CheatABC.switchChange(dtos[0].top) + CheatABC.switchChange(dtos[1].top) + CheatABC.switchChange(dtos[2].top);
        CheatABC.instance.SaveSums(sum);
    }
    public void SetDine(GenRandomDTO dto, int i)
    {
        //left
        GameObject gol = GameObject.FindGameObjectWithTag("d" + i + "l");
        UISprite sprite = gol.GetComponent<UISprite>();
        SetImage(sprite, dto.left);

        //top
        gol = GameObject.FindGameObjectWithTag("d" + i + "t");
        sprite = gol.GetComponent<UISprite>();
        SetImage(sprite, dto.top);
        result.Add(dto.top);
        //front
        gol = GameObject.FindGameObjectWithTag("d" + i + "f");
        sprite = gol.GetComponent<UISprite>();
        SetImage(sprite, dto.front);


    }
    void SetImage(UISprite sprite, int type)
    {

        switch (type)
        {
            case Const.BAU:
                sprite.spriteName = "Bau";
                break;
            case Const.CUA:
                sprite.spriteName = "Cua";
                break;
            case Const.TOM:
                sprite.spriteName = "Tom";
                break;
            case Const.CA:
                sprite.spriteName = "Ca";
                break;
            case Const.GA:
                sprite.spriteName = "Ga";
                break;
            case Const.NAI:
                sprite.spriteName = "Nai";
                break;
        }
    }
    public int IsWin(int type)
    {
        int r = 0;
        for (int i = 0; i < result.Count; i++)
        {
            if ((int)result[i] == type)
            {
                r++;
            }
        }
        if (r != 0)
        {
            return r;
        }
        return -1;
    }
}
