﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour
{
    public UILabel label;
    public int score;

    private const int START_COINS = 1000;
    private const int Gift_COINS = 1000;
    void Start()
    {
        score = PlayerPrefs.GetInt(Key.KEY_SCORE, START_COINS);
        setScore(score);
    }

    public void IncreateScore(int num)
    {
        score += num;
        setScore(score);
    }

    private void setScore(int _score)
    {
        score = _score;
        PlayerPrefs.SetInt(Key.KEY_SCORE, _score);
        label.text = score + "";
    }

    public void Gift()
    {
        Debug.Log("Gift Coins!");
        if (score <= 0)
        {
            setScore(Gift_COINS);
        }
    }
}
