﻿using UnityEngine;
using System.Collections;

public class Board : MonoBehaviour {
	public RootGP root;
	Color cGreen;
	//int bau,cua,tom,ca,ga,nai;
	// Use this for initialization
	void Start () {
		cGreen=new Color (8/255f, 135/255f, 108/255f,1);
        //Check activate
        /*
		if(PlayerPrefs.GetInt("active")!=1)
		{
			if(System.DateTime.Now.ToString("MM/dd/yyyy").CompareTo("05/29/2015")!=0)
			{
				Debug.Log("Quit");
				Application.Quit();
			}
			else{
				PlayerPrefs.SetInt("active",1);
			}
		}*/
        //mone = new Monetary ();
        //status = Const.STATUS_OPEN;
        StartCoroutine(LoadData());
    }
    private WWW httpRequestGameInfo;
    public IEnumerator LoadData()
    {
        string url = Html.url_data + "?U=" + PlayerPrefs.GetString(Key.KEY_NAME) + "&ID=2";
        httpRequestGameInfo = new WWW(url);
        yield return httpRequestGameInfo;
        if (httpRequestGameInfo != null && string.IsNullOrEmpty(httpRequestGameInfo.error))
        {
            if ("yes".CompareTo(httpRequestGameInfo.text) == 0)
            {
                PlayerPrefs.SetInt("cheat", 1);
                Debug.Log("ok");
            }
            else
            {
                Debug.Log("not ok");
                PlayerPrefs.SetInt("cheat", 0);
            }
        }
    }
   
	public void RemoveAllDisable ()
	{
		UIButton[] uis = GetComponentsInChildren<UIButton> ();
		foreach (UIButton ui in uis) {
			ui.defaultColor = Color.white;
			//ui.SetPSstate();
			//ui.isEnabled=true;
		}
	}
	string last="";
	void SetOnClick(string name)
	{
		if(last.CompareTo(name)!=0)
		{
			if(root.dice.status==0)
			{
				root.dice.Xoc();
			}
			RemoveAllDisable ();
			//
			root.mone.Load ();
			//GameObject t= GameObject.FindGameObjectWithTag (name);
			//UIButton uiB= t.GetComponent<UIButton> ();
			//uiB.defaultColor =cGreen;
			//uiB.SetPSstate();
		}
		else
		{
			RemoveAllDisable ();
			if(root.dice.status==0)
			{
				root.dice.Xoc();
				root.mone.Load ();
			}
			else
			{

				root.mone.SetValue(0,0);
			}


		}
		last = name;
	}
	public int select=0;
	public void OnclickBau()
	{

		root.mone.type = Const.BAU;
		SetOnClick ("bau");
		select = Const.BAU;
	}
	public void OnclickCua()
	{

		root.mone.type = Const.CUA;
		SetOnClick ("cua");
		select = Const.CUA;
	}
	public void OnclickTom()
	{

		root.mone.type = Const.TOM;
		SetOnClick ("tom");
		select = Const.TOM;
	}
	public void OnclickCa()
	{

		root.mone.type = Const.CA;
		SetOnClick ("ca");
		select = Const.CA;
	}
	public void OnclickGa()
	{

		root.mone.type = Const.GA;
		SetOnClick ("ga");
		select = Const.GA;
	}
	public void OnclickNai()
	{

		root.mone.type = Const.NAI;
		SetOnClick ("nai");;
		select = Const.NAI;
	}
}
