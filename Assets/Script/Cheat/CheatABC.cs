﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum CheatMode { Off, Cheat6, Cheat345}
public enum CheatKey { None, One, Two, Three, Four, Five, Six, Star}
public class CheatABC : MonoBehaviour
{
    private CheatMode currCheatMode;
    public CheatKey currentKey;
    private List<CheatKey> trigger345;

    public Camera uiCamera;
    public static CheatABC instance;
    public bool isCheat = false;
    public int lastSum, currSum;

    public int countForCheat = 0;
    private const int pivotCheatCount = 2;

    public List<CheatKey> cheatkeyStack = new List<CheatKey>();
    void Awake()
    {
        instance = this;
        isCheat = false;
        countForCheat = 0;
        currCheatMode = CheatMode.Cheat6;
        currentKey = CheatKey.None;
        trigger345 = new List<CheatKey>(){CheatKey.One, CheatKey.One, CheatKey.Two, CheatKey.Two};
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Ray ray = uiCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                GameObject go = hit.collider.gameObject;
                if (go.tag == "CheatButton")
                {
                    KeyContainer clickedKey = go.GetComponent<KeyContainer>();
                    if (clickedKey != null)
                    {
                        cheatkeyStack.Add(clickedKey.key);
                        currentKey = clickedKey.key;
                        Debug.Log("hit key: " + clickedKey.key);

                        switch (clickedKey.key)
                        {
                            case CheatKey.None:
                                cheatkeyStack.Clear();
                                break;
                            case CheatKey.One:
                                break;
                            case CheatKey.Two:
                                EnableCheat345Mode();
                                break;
                            case CheatKey.Three:
                                cheatkeyStack.Clear();
                                break;
                            case CheatKey.Four:
                                cheatkeyStack.Clear();
                                break;
                            case CheatKey.Five:
                                cheatkeyStack.Clear();
                                break;
                            case CheatKey.Six:
                                //setCurrCheatMode(CheatMode.Cheat6);
                                break;
                            case CheatKey.Star:
                                setCurrCheatMode(CheatMode.Off);
                                break;
                            default:
                                break;
                        }
                    }
                }
                
            }
        }
    }

    private void setCurrCheatMode(CheatMode _cheat)
    {
        currCheatMode = _cheat;

    }

    public CheatMode GetCurrCheatMode()
    {
        Debug.Log("CurrCheatMode: " + currCheatMode);
        return currCheatMode;
        
    }

    private void EnableCheat345Mode()
    {
        if(cheatkeyStack.Count > 3)
        {
            List<CheatKey> fourEmlements = cheatkeyStack.GetRange(cheatkeyStack.Count - 4, 4);
            bool result = true;
            for (int i = 0; i < fourEmlements.Count; i++)
            {
                if (fourEmlements[i] != trigger345[i])
                {
                    result = false;
                    break;
                }
            }

            if (result)
            {
                Debug.Log("On Mode 345");
                setCurrCheatMode(CheatMode.Cheat345);
            }

        }
     
    }



    public void SaveSums(int _sum)
    {
        lastSum = currSum;
        currSum = _sum;
        if (++countForCheat > pivotCheatCount)
        {
            isCheat = true;
        }
    }
    public int CalculateNext()
    {
        int result = 0;
        switch ((lastSum + currSum) % 6)
        {
            case 0: //Nai
                result = 5;
                break;
            case 1:  // Tom
                result = 2;
                break;
            case 2: //Ca
                result = 3;
                break;
            case 3: // ga
                result = 4;
                break;
            case 4: // Cua
                result = 1;
                break;
            case 5: // Bau 
                result = 0;
                break;
            default:
                result = 0;
                break;
        }
        return result;
    }

    /**  Quy uoc cua thang viet truoc
 * 
 * 	public const int BAU=0;
 *  public const int CUA=1;
 *  public const int TOM=2;
 *  public const int CA = 3;
 *  public const int GA=4;
 *  public const int NAI=5;
 * */

    //Như Nai 0; Tôm1; Cá2; Ga3; Cua 4; Bau 5.tổng 3 con chia 6 lấy số dư .​
    public static int switchChange(int oldConst)
    {
        int result = 0;
        switch (oldConst)
        {
            case 0:
                result = 5;
                break;
            case 1:
                result = 4;
                break;
            case 2:
                result = 1;
                break;
            case 3:
                result = 2;
                break;
            case 4:
                result = 3;
                break;
            case 5:
                result = 0;
                break;
            default:
                break;

        }
        return result;
    }

    public void CheateButtonClick()
    {
        Debug.Log("CheatButtonClick");
    }
}
