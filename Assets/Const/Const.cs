﻿using UnityEngine;
using System.Collections;

public class Const {
	//Define for Config
	public const string CONFIG_LANG_VI="CONFIG_LANG_VI";
	public const string CONFIG_LANG_ENG="CONFIG_LANG_ENG";
	//End
	public const int BAU=0;
	public const int CUA=1;
	public const int TOM=2;
	public const int CA = 3;
	public const int GA=4;
	public const int NAI=5;
	//Status game
	public const int STATUS_XOC=0;
	public const int STATUS_OPEN=1;
	public const int STATUS_PICK=2;
	
}
