﻿using UnityEngine;
using System.Collections;

public class Key {
	public const string KEY_SOUND_ON_OFF="KEY_SOUND_ON_OFF";
	public const string KEY_SOUND_VOL="KEY_SOUND_VOL";
	public const string KEY_USER="KEY_USER";
	public const string KEY_FIRST="KEY_FIRST";
	public const string KEY_LOGIN="KEY_LOGIN";
	public const string KEY_NAME="KEY_NAME";
	public const string KEY_SCORE="KEY_SCORE";
	public const string KEY_FIRST_REWARD="KEY_FIRST_REWARD";
	public const string KEY_PRICE_RATE="KEY_PRICE_RATE";
	public const string KEY_PRICE_ADS="KEY_PRICE_ADS";
	//User define

}
