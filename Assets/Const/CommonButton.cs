﻿using UnityEngine;
using System.Collections;

public class CommonButton : MonoBehaviour {
	public RootGP root;
	public string level;
	int count=0;
	bool reward;
	// Use this for initialization
	void Start () {
		reward = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(reward)
		{
			count++;
			if(count==300)
			{
				root.score.IncreateScore (1500);
				PlayerPrefs.SetInt(Key.KEY_FIRST_REWARD,1);
				reward=false;
			}
		}
	}
	public void OnClickLoadLevel()
	{
		if (level != null) {
			Application.LoadLevel (level);
		}
	}
	public void OnClickLoadLevelWithAds()
	{
		if (level != null) {
			Application.LoadLevel (level);
		}
	}
	public void OnClickRateShow()
	{
		UniRate.Instance.ShowPrompt ();
		if(PlayerPrefs.GetInt(Key.KEY_FIRST_REWARD)==0)
		{
			reward=true;
		}
	}
	public void OnClickQuit()
	{
		Application.Quit ();
	}
}
