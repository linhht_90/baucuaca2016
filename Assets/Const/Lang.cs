﻿using UnityEngine;
using System.Collections;

public class Lang {
	public static string Rate_Title_Message()
	{
		switch (Config.language) {
		case Const.CONFIG_LANG_VI:
			return "Đánh giá game!!!";
		case Const.CONFIG_LANG_ENG:
			return "Rate this game!!!";
		}
		return "";
	}
	public static string Rate_Message()
	{
		switch (Config.language) {
		case Const.CONFIG_LANG_VI:
			return "Bạn có muốn đánh giá game này?";
		case Const.CONFIG_LANG_ENG:
			return "Do you wanna rate this game?";
		}
		return "";
	}
	public static string Rate_Button_OK()
	{
		switch (Config.language) {
		case Const.CONFIG_LANG_VI:
			return "Đồng ý";
		case Const.CONFIG_LANG_ENG:
			return "OK";
		}
		return "";
	}
	public static string Rate_Button_Cancel()
	{
		switch (Config.language) {
		case Const.CONFIG_LANG_VI:
			return "Không";
		case Const.CONFIG_LANG_ENG:
			return "No";
		}
		return "";
	}
	public static string Rate_Button_Remind()
	{
		switch (Config.language) {
		case Const.CONFIG_LANG_VI:
			return "Nhắc sau";
		case Const.CONFIG_LANG_ENG:
			return "Remind";
		}
		return "";
	}

}
