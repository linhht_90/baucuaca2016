﻿using UnityEngine;
using System.Collections;

public class Store : MonoBehaviour {
	public RootGP root;
	public SpringPosition spring;
	public GameObject rate;
	public GameObject adsA;
	public GameObject adsS;
	public CommonButton button;
	// Use this for initialization
	void Start () {
		PlayerPrefs.SetInt (Key.KEY_PRICE_RATE, 0);
		PlayerPrefs.SetInt (Key.KEY_PRICE_ADS, 0);
	}
	
	// Update is called once per frame
	void Update () {
		if(PlayerPrefs.GetInt(Key.KEY_PRICE_RATE)==1)
		{
			rate.SetActive(false);
		}
		else
		{
			rate.SetActive(true);
		}
		if(PlayerPrefs.GetInt(Key.KEY_PRICE_ADS)!=1)
		{
			adsA.SetActive(true);
		}
		else
		{
			adsA.SetActive(false);
		}
			
	}
	public void Load()
	{
		root.dialog.first = true;
		spring.target = new Vector3 (0, 8.43f, 0);
		spring.enabled = true;

	}
	public void UnLoad()
	{
		root.dialog.first = false;
		spring.target = new Vector3 (0, -500, 0);
		spring.enabled = true;

	}
	public void OnlickRate()
	{
		PlayerPrefs.SetInt (Key.KEY_PRICE_RATE, 1);
		button.OnClickRateShow ();

	}
	public void OnclickAdsA()
	{
		PlayerPrefs.SetInt (Key.KEY_PRICE_ADS, 1);
		root.score.IncreateScore (1500);
	}
	public void OnclickAdsS()
	{

		PlayerPrefs.SetInt (Key.KEY_PRICE_ADS, 0);
	}
}
