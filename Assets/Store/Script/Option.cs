﻿using UnityEngine;
using System.Collections;

public class Option : MonoBehaviour {
	public GameObject sell;
	public GameObject buy;
	public GameObject svsell;
	public GameObject svbuy;
	UISprite uiSell;
	UISprite uiBuy;
	int type;
	// Use this for initialization
	void Start () {
		type = 1;
		uiSell = sell.GetComponentInChildren<UISprite> ();
		uiBuy= buy.GetComponentInChildren<UISprite> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(type==0)
		{
			uiBuy.spriteName = "tab_bt_0";
			uiSell.spriteName = "tab_bt_1";

		}
		else
		{
			uiBuy.spriteName = "tab_bt_1";
			uiSell.spriteName = "tab_bt_0";

		}
	}
	public void OnclickBuy()
	{
		svsell.SetActive(true);
		svbuy.SetActive(false);
		type = 0;
	}
	public void OnclickSell()
	{
		svsell.SetActive(false);
		svbuy.SetActive(true);
		type = 1;
	}
}
