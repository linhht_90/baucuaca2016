﻿using UnityEngine;
using System.Collections;

public class LoginButton : MonoBehaviour {
	//public Login login;
	public UILabel name;
	public UILabel warning;
	private WWW httpRequestGameInfo;
	int loadsucess;
	// Use this for initialization
	void Start () {
		loadsucess = -2;
	}
	
	// Update is called once per frame
	float elapsedTime;
	float waitTime=10;
	void Update () {
		if(loadsucess==0)
		{
			elapsedTime += Time.deltaTime;
			if (elapsedTime >= waitTime)
			{
				StopCoroutine("loadGameInfo");
				httpRequestGameInfo.Dispose();
				httpRequestGameInfo = null;
				loadsucess=-1;
			}
		}
		if(loadsucess==1)
		{
			warning.text="";
		}
		if(loadsucess==-1)
		{
			warning.text="Kiểm tra mạng và tải lại!!!";
		}
	}
	public void OnclickOk()
	{

		if (loadsucess != -2) {
				}
		loadsucess = 0;
		string t = name.text.Replace ("_", "");
		if(t.Length<4)
		{
			warning.color = Color.red;
			warning.text="*Nhỏ hơn 4 kí tự!";
			loadsucess=-2;
			return;
		}
		if (t.Length > 12) {
			warning.color = Color.red;
			warning.text="*Lớn hơn 12 kí tự!";
			loadsucess=-2;
			return;
		}
		StartCoroutine(LoadHTML (t));



	}
	public IEnumerator LoadHTML(string t)
	{
		string url = Html.url+"query.php?U="+t;
		warning.color = Color.green;
		warning.text = "Tải...";
		httpRequestGameInfo = new WWW(url);
		yield return httpRequestGameInfo;
		if (httpRequestGameInfo != null && string.IsNullOrEmpty(httpRequestGameInfo.error))
		{
			if(httpRequestGameInfo.text.CompareTo("yes")==0)
			{
				PlayerPrefs.SetString(Key.KEY_NAME,t);
				PlayerPrefs.SetInt(Key.KEY_LOGIN,1);
				Application.LoadLevel ("rating");
				//login.UnLoadLogin ();
				//login.Finish();

				loadsucess=1;
			}
			else
			{
				loadsucess=-2;
				warning.color = Color.red;
				warning.text="*Tên đã được sử dụng";
			}
		}
	}
}
