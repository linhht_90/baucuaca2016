﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
public class Rating : MonoBehaviour {
	private WWW httpRequestGameInfo;
	private string position;
	private ArrayList listUser;
	public GameObject root;
	public UILabel load;
	public UILabel you;
	public GameObject login;
	int loadsucess;
	const float waitTime=10;
	// Use this for initialization
	void Start () {
		loadsucess = -2;
		if (PlayerPrefs.GetInt (Key.KEY_LOGIN) == 1) {
			login.SetActive(false);
			Refresh ();
		}
	}
	public void Refresh()
	{
		listUser = new ArrayList ();
		StartCoroutine(LoadHTML ());
		loadsucess = 0;
		elapsedTime = 0;
		top5 = false;
	}
	float count;
	float elapsedTime;
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)) { Application.LoadLevel("play"); }
		if(loadsucess==0)
		{
			load.text="Đang tải...";
			elapsedTime += Time.deltaTime;
			if (elapsedTime >= waitTime)
			{
				StopCoroutine("loadGameInfo");
				httpRequestGameInfo.Dispose();
				httpRequestGameInfo = null;
				loadsucess=-1;
			}
		}
		if(loadsucess==1)
		{
			load.text="";
		}
		if(loadsucess==-1)
		{
			load.text="Kiểm tra mạng và tải lại.";
		}
	}
	bool onclick=false;
	public void OnClick()
	{
		onclick = true;
	}
	public IEnumerator LoadHTML()
	{
		string url = Html.url + "?U=" + PlayerPrefs.GetString (Key.KEY_NAME) + "&S=" + PlayerPrefs.GetInt (Key.KEY_SCORE);
		httpRequestGameInfo = new WWW(url);
		yield return httpRequestGameInfo;
		if (httpRequestGameInfo != null && string.IsNullOrEmpty(httpRequestGameInfo.error))
		{
			ProcessData(httpRequestGameInfo.text);
		}
	}
	private const float top=100f;
	private bool top5;
	private void ProcessData(string text)
	{
		text+=" ... ... "+PlayerPrefs.GetString(Key.KEY_NAME)+" "+PlayerPrefs.GetInt(Key.KEY_SCORE);
		string[] array= text.Split();
		you.text =""+array [0];
		loadsucess = 1;
		int tt =int.Parse(array[0]);
		for(int i=1;i<array.Length-1;i+=2)
		{
			listUser.Add(new DataRating(array[i],array[i+1]));
			GameObject pos = (GameObject)Instantiate (Resources.Load ("Item0", typeof(GameObject)));
			pos.transform.parent=root.transform;
			pos.transform.localPosition=new Vector3(0,top-(25*i/2),0);
			pos.transform.localScale=new Vector3(1,1,1);
			UILabel[] stt=pos.GetComponentsInChildren<UILabel>();
			UISprite sprite=pos.GetComponentInChildren<UISprite>();
			//pos.transform.parent=transform;



			foreach (UILabel label in stt) {
				switch(label.text)
				{
				case "name":
					label.text=array[i];
					break;
				case "stt":
					if((i+1)/2==7)
					{
						label.text=""+tt;
					}
					else{
						label.text=(i/2+1)+"";
					}
					break;
				case "score":
					label.text=array[i+1];
					break;
				}
				if(i==1)
				{
					label.color=Color.red;
					//sprite.spriteName="bar_red";
				}
				if(((i+1)/2)==tt)
				{
					top5=true;
					label.color=Color.green;
				}
				if((i+1)/2==7)
				{
					if(!top5)
					{
						label.color=Color.green;
					}
					else
					{
						GameObject.Destroy(pos);
						continue;
					}
				}

			}

		}
		


	}

}