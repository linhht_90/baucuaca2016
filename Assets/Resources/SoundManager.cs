﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{

    private static SoundManager _instance;
    private static object _lock = new object();

    public AudioSource sourceSound;
    public AudioSource sourceMusic;

    public AudioClip clickBtnSound;
    public AudioClip introSound;
	public AudioClip shake;
	public AudioClip coin;
	public AudioClip welldone;
    public AudioClip[] bgSounds;

    public static SoundManager Instance
    {
        get
        {
            lock (_lock)
            {
                if (_instance == null)
                {
                    Object soundObj = Resources.Load("SoundManager");
                    if (soundObj != null)
                    {
                        GameObject obj = Instantiate(soundObj) as GameObject;
                        if (obj != null)
                        {
                            _instance = obj.GetComponent<SoundManager>();
                            DontDestroyOnLoad(_instance.gameObject);
                        }
                    }
                }
                return _instance;
            }
        }
    }

    void Start()
    {
        reloadFromSettings();

    }

    public static void reloadFromSettings()
    {
        //Instance.sourceMusic.volume = StUserSettings.Music;
        //Instance.sourceSound.volume = StUserSettings.Sound;
    }

    public void onClickBtnSystem()
    {
        playOneShotClick();
    }

    static public bool mutedMusic
    {
        get
        {
            if (Instance != null)
            {
                return Instance.sourceMusic.mute;
            }
            return true;
        }
        set
        {
            if (Instance != null)
            {
                Instance.sourceMusic.mute = value;
            }
        }
    }

    static public bool mutedSound
    {
        get
        {
            if (Instance != null)
            {
                return Instance.sourceSound.mute;
            }
            return true;
        }
        set
        {
            if (Instance != null)
            {
                Instance.sourceSound.mute = value;
            }
        }
    }

    public static void playOneShot(AudioClip sound)
    {
        if (Instance != null)
        {
            Instance.sourceSound.PlayOneShot(sound);
        }
    }

    public static void playOneShotClick()
    {
        if (Instance != null)
        {
            playOneShot(Instance.clickBtnSound);
        }
    }

    public static void playIntro()
    {
        if (Instance != null)
        {
            playMusic(Instance.introSound);
        }
    }

    public static void playBgRandom()
    {
        if (Instance != null)
        {
            int soundIndex = Random.Range(0, 5);
            if (soundIndex < Instance.bgSounds.Length)
            {
                playMusic(Instance.bgSounds[soundIndex]);
            }
            else
            {
                playMusic(Instance.introSound);
            }

        }
    }

    public static void playMusic(AudioClip sound)
    {
        if (Instance != null)
        {
            Instance.sourceMusic.loop = true;
            Instance.sourceMusic.clip = sound;
            Instance.sourceMusic.Play();
        }
    }

    public static void stopMusic()
    {
        if (Instance != null)
        {
            Instance.sourceMusic.Stop();
        }
    }
}
